#include <jansson.h>
#include <string>
#include <vector>
using namespace std;


class itCompany{
	

	public:
	itCompany();
	
	// void setName(const char * name);
	// void setPeople(int people);
	// void setSalary(double salary);

	// string getName();
	// int getPeople();
	// double getSalary();

	char * name;
	int people;
	double midSalary;

};


void Storage_saveToJSON(const char * fileName, vector<itCompany *>  comps);

vector<itCompany*> Storage_loadFromJSON(const char * fileName, vector<itCompany *>  comps);

bool readAllText(const char * filename, char * text);
bool compareVectors(vector<itCompany*> v1, vector<itCompany*> v2);
bool compareComps(itCompany c1, itCompany c2);

void _printToFile(const char * fileName, char * buffer);



