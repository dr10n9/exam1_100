#include "storage.h"
#include <stdlib.h>
#include <string.h>
#include <jansson.h>
#include <fstream>
bool compareComps(itCompany * c1, itCompany * c2){
	//printf("%i || %i ; %f || %f", c1->people, c2->people, c1->midSalary, c2->midSalary);
	if(c1->people == c2->people && c1->midSalary==c2->midSalary){
		return true;
	}
	return false;
}

bool compareVectors(vector<itCompany*> v1, vector<itCompany*> v2){
	// printf("||| %i\n", (int)v1.size());
	// printf("||| %i\n", (int)v2.size());
	if(v1.size() == v2.size()){
		for(int i = 0; i<v1.size() ; i++ ){
			bool check = compareComps(v1[i], v2[i]);
			if(!check){
				
				return false;
			}
		}
		return true;
	}
	return false;
}

void _printToFile(const char * fileName, char * buffer) {
        FILE* fout = fopen(fileName, "w");
        fprintf(fout, "%s\n", buffer);
        fclose(fout);
}

bool readAllText(const char * filename, char * text) {
        char line[100];
    
    FILE * fr = fopen(filename, "r");
        if (fr == NULL) return false;
    while(fgets(line, 100, fr)) {
        strcat(text, line);
    }
        fclose(fr);
        return true;
}

void Storage_saveToJSON(const char * fileName, vector<itCompany* > comps){
		json_t * json = json_object();
        char jsonText[10000] = "[\n\0";
        for (int i = 0; i < comps.size(); i++) {
			itCompany * comp =  comps.at(i);
			
			json_object_set_new(json, "name", json_string(comp->name));
			json_object_set_new(json, "people", json_integer(comp->people));
			json_object_set_new(json, "salary", json_real(comp->midSalary));
			char * jsonString = json_dumps(json, JSON_INDENT(4));
                strcat(jsonText, jsonString);
                strcat(jsonText, ",\n");
                free(jsonString);
		}
		//puts(jsonText);
		jsonText[strlen(jsonText) - 2] = ' ';
        strcat(jsonText, "]");
		_printToFile(fileName, jsonText);
		json_decref(json);
};


vector<itCompany*> Storage_loadFromJSON(const char * fileName, vector<itCompany *>  comps){
	char jsonStr[1000] = "";
	readAllText(fileName, jsonStr);
	json_error_t err;
        json_t * jsonArr = json_loads(jsonStr, 0, &err);
        int index = 0;
        json_t * value = NULL;
		json_array_foreach(jsonArr, index, value){
			
			itCompany * comp = new itCompany();
			//puts(comp->name);
			//printf("in file: ");
			//puts(json_string_value(json_object_get(value, "name")));
			//printf("in json: %i\n", (int)json_integer_value(json_object_get(value, "people")));
			
			//puts("as");
			//puts(json_integer_value(json_object_get(value, "people")));
			//strcpy(comp->name, (char *)json_string_value(json_object_get(value, "name")));
			comp->name = (char*)json_string_value(json_object_get(value, "name"));
			comp->people = json_integer_value(json_object_get(value, "people"));
			comp->midSalary = json_real_value(json_object_get(value, "salary"));
			//printf("to push: %s, %i, %f\n", comp->name, comp->people, comp->midSalary);

			comps.push_back(comp);
			
			
		}
		index++;
		
        json_decref(jsonArr);
		//printf("loaded vec size = %i\n", (int)comps.size());
		return comps;
};

// string itCompany::getName(){
// 	return this->name;
// }

// int itCompany::getPeople(){
// 	return this->people;
// }

// double itCompany::getSalary(){
// 	return this->midSalary;
// }

// void itCompany::setName(string name){
// 	this->name=name;
// }

// void itCompany::setPeople(int people){
// 	this->people=people;
// }

// void itCompany::setSalary(double salary){
// 	this->midSalary=salary;
// }
itCompany::itCompany(){
	this->name=" ";
	this->people=0;
	this->midSalary=0.0;
}






