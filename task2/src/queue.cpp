#include <queue.h>

Queue::Queue(){
	for(int i = 0; i<10; i++){
		queue[i]="\0";
	}
}

bool Queue::enqueue(string str){
	if(size == 0){
		queue[0] = str;
		tail = str;
		head = str;
		size++;
		return true;
	}
	if(size>0 && size < 10){
		queue[size] = str;
		tail = str;
		size++;
	}
	return false;
}

string Queue::dequeue(){
	string str = "\0";
	if(size == 0){
		return "QUEUE IS EMPTY";
	}

	str = head;
	for(int i = 0; i<size-1; i++){
		queue[i]=queue[i+1];
	}
	queue[size-1]="\0";
	size--;
	return str;
}

int Queue::sizeOf(){
	return this->size;
}

