#include <student.h>
 
Student * Student_new(char * firstName, char * lastName, int age, float score){
    Student * self = malloc(sizeof(Student));
    strcpy(self->firstName, firstName);
    strcpy(self->lastName, lastName);
    self->age = age;
    self->score = score;
    return self;
}
 
void Student_print(Student * self){
    printf("\nFirst name: %s, Last name: %s, Age: %i, Score: %g\n",
        self->firstName,
        self->lastName,
        self->age,
        self->score
        );
        //Planet_free();
}
 
Student * Student_newFromString(char * str) {
    Student * self = Student_new("", "", 0, 0.0);
    sscanf(str, "%s %s %i %f",
        self->firstName,
        self->lastName,
        &(self)->age,
        &(self)->score);
    return self;
}
 
char * Student_toString(Student * self, char * buffer) {
    sprintf(buffer, "%s %s %i %f",
        self->firstName,
        self->lastName,
        self->age,
        self->score);
    return buffer;
}