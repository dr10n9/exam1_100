#ifndef QUEUE_H
#define QUEUE_H

#include <string>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
class Queue{
	private:
	string queue[10];
	int size;
	string head;
	string tail;
	public:
	Queue();
	int sizeOf();
	string dequeue();
	bool enqueue(string str);

};

#endif