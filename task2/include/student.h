#ifndef STUDENT_H
#define STUDENT_H
 
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
 
typedef struct {
    char firstName[50];
    char lastName[50];
    int age;
    float score;
} Student;
 
 
Student * Student_new(char * firstName, char * lastName, int age, float score);
Student * Student_newFromString(char * str);
void Student_print(Student * self);
char * Student_toString(Student * self, char * buffer);
 
#endif